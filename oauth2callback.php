<?php
//taken from https://developers.google.com/api-client-library/php/auth/web-app
require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client(); //again because of the redirect, in php every page we need to create object
$client->setAuthConfigFile('client_secrets.json');
$client->setRedirectUri('http://hilasc.myweb.jce.ac.il/calendar/oauth2callback.php');
$client->addScope('https://www.googleapis.com/auth/calendar'); // all actions in calender

if (! isset($_GET['code'])) { // is there a code from google? temporary code (first time is no so we enter the if)
  $auth_url = $client->createAuthUrl(); //creates the USR 
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL)); //redirect , auth_url is in google's server
} else { //if there is a code (after the user accepts the app)
  $client->authenticate($_GET['code']); // the clien contact the server (google) with out the redirect (header)
  $_SESSION['access_token'] = $client->getAccessToken(); //contact google server, give him the temporary code (to show foofle he is the same client) and then client gets the permanent code
  $redirect_uri = 'http://hilasc.myweb.jce.ac.il/calendar'; 
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL)); //redirect to the client (back to index)
}