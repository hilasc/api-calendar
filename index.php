<?php

require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secrets.json');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (isset($_SESSION['access_token']) && $_SESSION['access_token'] &&  !empty($_POST['startDate']) && !empty($_POST['endDate'])) { //do we have access token from google? (first time in google we don't have the token so -> else)
																	// after we got back from oauth2callback, the if is yes so we enter the if
  $client->setAccessToken($_SESSION['access_token']);
  $service = new Google_Service_Calendar($client);

  $event = new Google_Service_Calendar_Event(array(
  'summary' => 'test event 2017',
  'location' => 'Jerusalem',
  'description' => 'Testing the google calendar API',
  'start' => array(
    'dateTime' => $_POST["startDate"]. ':00',
    'timeZone' => 'Asia/Jerusalem',
  ),
  'end' => array(
    'dateTime' => $_POST["endDate"]. ':00',
    'timeZone' => 'Asia/Jerusalem',
  ),
  
  /*'start' => array(
    'dateTime' => '2017-02-28T14:00:00',
    'timeZone' => 'Asia/Jerusalem',
  ),
  'end' => array(
    'dateTime' => '2017-02-28T15:00:00',
    'timeZone' => 'Asia/Jerusalem',
  ),*/
  /* 'start' => array(
    'dateTime' => '2017-02-02T09:00:00-7:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'end' => array(
    'dateTime' => '2017-02-02T10:00:00-7:00',
    'timeZone' => 'America/Los_Angeles',
  ),
  'recurrence' => array(
    'RRULE:FREQ=DAILY;COUNT=2'
  ),*/
  'attendees' => array(
    array('email' => 'lpage@example.com'),
    array('email' => 'sbrin@example.com'),
  ),
  'reminders' => array(
    'useDefault' => FALSE,
    'overrides' => array(
      array('method' => 'email', 'minutes' => 24 * 60),
      array('method' => 'popup', 'minutes' => 10),
    ),
  ),
));
  
$calendarId = 'primary';
$event = $service->events->insert($calendarId, $event);
printf('Event created: %s\n', $event->htmlLink);
  
} elseif (!isset($_SESSION['access_token']) || !$_SESSION['access_token']){
  $redirect_uri =  'http://hilasc.myweb.jce.ac.il/calendar/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL)); //go to oAuth2callback

}

else {?>

	<div>
			<h2>Create an event</h2>
			<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
				Start:  <input type="datetime-local" name="startDate"></br>
				End: <input type="datetime-local" name="endDate"></br></br>
				
				<input type="submit" name="submit" value="Create">
				<input type="reset" value="Reset">
			</form>
	</div>

<?php } ?>